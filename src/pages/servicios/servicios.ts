import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ShowServicioPage } from '../show-servicio/show-servicio';

import { Servicio, ServiciosProvider } from '../../providers/servicios/servicios';

/**
 * Generated class for the ServiciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-servicios',
  templateUrl: 'servicios.html',
})
export class ServiciosPage {
  public servicios:Servicio[] = [];

  private serviciosProvider:ServiciosProvider;

  constructor(public navCtrl: NavController, public navParams: NavParams, serviciosProvider:ServiciosProvider) {
    this.serviciosProvider = serviciosProvider;
  }

  ionViewDidLoad() {
    this.start();
  }

  showServicio(index:number){
    this.navCtrl.push(ShowServicioPage, {index: index});
  }

  start(){
    this.servicios = this.serviciosProvider.getServicios();
  }

}
