import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CursoShowPage } from './curso-show';

@NgModule({
  declarations: [
    CursoShowPage,
  ],
  imports: [
    IonicPageModule.forChild(CursoShowPage),
  ],
})
export class CursoShowPageModule {}
