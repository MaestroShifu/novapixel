import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import provider
import { Curso,CursosProvider } from '../../providers/cursos/cursos';

/**
 * Generated class for the CursoShowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-curso-show',
  templateUrl: 'curso-show.html',
})
export class CursoShowPage {
  public curso:Curso;

  private cursoProvider:CursosProvider;

  constructor(public navCtrl: NavController, public navParams: NavParams, cursoProvider:CursosProvider) {
    this.cursoProvider = cursoProvider;

    this.curso = this.cursoProvider.getCursoFind(this.navParams.get('index'));

    console.log(this.curso);
  }

  ionViewDidLoad() {
    
  }

}
