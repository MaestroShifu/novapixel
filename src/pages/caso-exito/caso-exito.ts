import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//Pagina
import { CasosShowPage } from '../casos-show/casos-show';
//Provider
import { CasosExitosProvider, Casos } from '../../providers/casos-exitos/casos-exitos';

/**
 * Generated class for the CasoExitoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-caso-exito',
  templateUrl: 'caso-exito.html',
})
export class CasoExitoPage {

  private casosExitosProvider:CasosExitosProvider
  public casos:Casos[] = [];


  constructor(public navCtrl: NavController, public navParams: NavParams, casosExitosProvider:CasosExitosProvider) {
  
    this.casosExitosProvider = casosExitosProvider;
  this.casos = this.casosExitosProvider.getCasos();

  }

  ionViewDidLoad() {
   
  }

  showCaso(index:number){
    this.navCtrl.push(CasosShowPage, {index: index});
  }

}
