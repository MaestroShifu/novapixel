import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CasoExitoPage } from './caso-exito';

@NgModule({
  declarations: [
    CasoExitoPage,
  ],
  imports: [
    IonicPageModule.forChild(CasoExitoPage),
  ],
})
export class CasoExitoPageModule {}
