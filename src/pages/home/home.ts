import { Component } from '@angular/core';
import { Nav, NavController } from 'ionic-angular';

//import providers
import { CursosProvider } from '../../providers/cursos/cursos';
import { CasosExitosProvider } from '../../providers/casos-exitos/casos-exitos';
import { TeamNovapixelProvider } from '../../providers/team-novapixel/team-novapixel';

//import pages
import { NosotrosPage } from '../nosotros/nosotros';
import { CursosPage } from '../cursos/cursos';
import { ServiciosPage } from '../servicios/servicios';
import { CasoExitoPage } from '../caso-exito/caso-exito';
import { TeamNovapixelPage } from '../team-novapixel/team-novapixel';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private cursosProvider:CursosProvider;
  private casosExitosProvider:CasosExitosProvider;
  private teamNovapixelProvider:TeamNovapixelProvider;
  
  private nav:Nav;

  public nosotros:any = NosotrosPage;
  public cursos:any = CursosPage;
  public servicios:any = ServiciosPage;
  public casos:any = CasoExitoPage;
  public team:any = TeamNovapixelPage;

  constructor(public navCtrl: NavController, cursosProvider:CursosProvider, teamNovapixelProvider:TeamNovapixelProvider, casosExitosProvider:CasosExitosProvider, nav:Nav) {
    this.nav = nav;
    this.cursosProvider = cursosProvider;
    this.casosExitosProvider = casosExitosProvider;
    this.teamNovapixelProvider = teamNovapixelProvider;

  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page);
  }

}
