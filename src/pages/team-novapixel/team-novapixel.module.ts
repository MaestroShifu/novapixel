import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamNovapixelPage } from './team-novapixel';

@NgModule({
  declarations: [
    TeamNovapixelPage,
  ],
  imports: [
    IonicPageModule.forChild(TeamNovapixelPage),
  ],
})
export class TeamNovapixelPageModule {}
