import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TeamNovapixelProvider, Departamento } from '../../providers/team-novapixel/team-novapixel';
/**
 * Generated class for the TeamNovapixelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-team-novapixel',
  templateUrl: 'team-novapixel.html',
})
export class TeamNovapixelPage {


  public departamentos:Departamento[] = [];

  private teamNovapixelProvider:TeamNovapixelProvider;

  constructor(public navCtrl: NavController, public navParams: NavParams, teamNovapixelProvider:TeamNovapixelProvider) {

    this.teamNovapixelProvider = teamNovapixelProvider;
  }

  ionViewDidLoad() {
    this.start();
  }

  start(){
    this.departamentos = this.teamNovapixelProvider.getDepartamento();
  }
}
