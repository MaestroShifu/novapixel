import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//import pages
import { CursoShowPage } from '../curso-show/curso-show';

//import providers
import { CursosProvider, Curso } from '../../providers/cursos/cursos';

/**
 * Generated class for the CursosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cursos',
  templateUrl: 'cursos.html',
})
export class CursosPage {
  
  public cursos:Curso[] = [];

  private cursosProvider:CursosProvider;

  constructor(public navCtrl: NavController, public navParams: NavParams, cursosProvider:CursosProvider) {
    this.cursosProvider = cursosProvider;
  }

  ionViewDidLoad() {
    this.start();
  }

  start(){
    this.cursos = this.cursosProvider.getCursos();
  }

  showCurso(index:number){
    this.navCtrl.push(CursoShowPage, {index: index});
  }

}
