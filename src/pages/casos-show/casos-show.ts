import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//Provider
import { Casos, CasosExitosProvider} from '../../providers/casos-exitos/casos-exitos';

/**
 * Generated class for the CasosShowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-casos-show',
  templateUrl: 'casos-show.html',
})
export class CasosShowPage {

  public caso:Casos;
  private CasosExitosProvider:CasosExitosProvider

  constructor(public navCtrl: NavController, public navParams: NavParams, CasosExitosProvider:CasosExitosProvider) {
    
    this.CasosExitosProvider = CasosExitosProvider;

    this.caso = this.CasosExitosProvider.getCasoFind(this.navParams.get('index'));
  
  }

  ionViewDidLoad() {
    
  }

}
