import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CasosShowPage } from './casos-show';

@NgModule({
  declarations: [
    CasosShowPage,
  ],
  imports: [
    IonicPageModule.forChild(CasosShowPage),
  ],
})
export class CasosShowPageModule {}
