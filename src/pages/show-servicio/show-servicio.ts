import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Servicio, ServiciosProvider } from '../../providers/servicios/servicios';

/**
 * Generated class for the ShowServicioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-show-servicio',
  templateUrl: 'show-servicio.html',
})
export class ShowServicioPage {

  public servicio:Servicio;
  private servicioProvider:ServiciosProvider;

  constructor(public navCtrl: NavController, public navParams: NavParams, servicioProvider:ServiciosProvider) {
    this.servicioProvider = servicioProvider;

    this.servicio = this.servicioProvider.getServiciosFind(this.navParams.get("index"));
  }

  ionViewDidLoad() {
    
  }

}
