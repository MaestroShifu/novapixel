import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowServicioPage } from './show-servicio';

@NgModule({
  declarations: [
    ShowServicioPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowServicioPage),
  ],
})
export class ShowServicioPageModule {}
