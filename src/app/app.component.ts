import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import pages
import { HomePage } from '../pages/home/home';
import { CursosPage } from '../pages/cursos/cursos';
import { CasoExitoPage } from '../pages/caso-exito/caso-exito';
import { NosotrosPage } from '../pages/nosotros/nosotros';
import { ServiciosPage } from '../pages/servicios/servicios';
import { TeamNovapixelPage } from '../pages/team-novapixel/team-novapixel';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Cursos', component: CursosPage },
      { title: 'Servicios', component: ServiciosPage },
      { title: 'Caso de Exito', component: CasoExitoPage },
      { title: 'Nosotros', component: NosotrosPage },
      { title: 'Nuestro Equipo', component: TeamNovapixelPage },
      
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
