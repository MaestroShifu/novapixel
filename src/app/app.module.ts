import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

//import paginas
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CursosPage } from '../pages/cursos/cursos';
import { CasoExitoPage } from '../pages/caso-exito/caso-exito';
import { NosotrosPage } from '../pages/nosotros/nosotros';
import { ServiciosPage } from '../pages/servicios/servicios';
import { CursoShowPage } from '../pages/curso-show/curso-show';
import { CasosShowPage } from '../pages/casos-show/casos-show' ;
import { FabsPage } from '../pages/fabs/fabs';
import { TeamNovapixelPage } from '../pages/team-novapixel/team-novapixel';
import { ShowServicioPage } from '../pages/show-servicio/show-servicio';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import rpoviders
import { CursosProvider } from '../providers/cursos/cursos';
import { CasosExitosProvider } from '../providers/casos-exitos/casos-exitos';
import { TeamNovapixelProvider } from '../providers/team-novapixel/team-novapixel';
import { ServiciosProvider } from '../providers/servicios/servicios';

@NgModule({
  declarations: [
    FabsPage,
    MyApp,
    HomePage,
    CursosPage,
    CasoExitoPage,
    NosotrosPage,
    ServiciosPage,
    CursoShowPage,
    CasosShowPage,
    TeamNovapixelPage,
    ShowServicioPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    FabsPage,
    MyApp,
    HomePage,
    CursosPage,
    CasoExitoPage,
    NosotrosPage,
    ServiciosPage,
    CursoShowPage,
    CasosShowPage,
    TeamNovapixelPage,
    ShowServicioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CursosProvider,
    CasosExitosProvider,
    TeamNovapixelProvider,
    ServiciosProvider
  ]
})
export class AppModule {}
