import { Injectable } from '@angular/core';

/*
  Generated class for the TeamNovapixelProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TeamNovapixelProvider {

  private departamentos:Departamento[] = [];

  constructor() {
   this.start();
  }

  getDepartamento(){
    return this.departamentos;
  }

  setDepartamento(departamento:Departamento){
    this.departamentos.push(departamento);
  }


start(){
  let developer:Team[] = [{
    nombre: 'Francisco Sanchez',
    imagen: '../../assets/imgs/team/francesco.jpg',
    cargo: 'Software Developer',
  },{
    nombre: 'Daniel Santos',
    imagen: '../../assets/imgs/team/daniel2.jpg',
    cargo: 'Software Developer',
  },{
    nombre: 'Nataly Vergel',
    imagen: '../../assets/imgs/team/nataly.jpg',
    cargo: 'Software Developer',
  },{
    nombre: 'Daniel Luis',
    imagen: '../../assets/imgs/team/daniel.jpg',
    cargo: 'Software Developer',
  }];

  let creative:Team[] = [{
    
    nombre: 'Stiven Vargas',
    imagen: '../../assets/imgs/team/johan.jpg',
    cargo: 'Publicidad y Marketing Digital',
  },{
    nombre: 'Luis Ortiz',
    imagen: '../../assets/imgs/team/luis.jpg',
    cargo: 'Publicidad y Marketing Digital',
  }];

  let ventas:Team[] = [{
      nombre: 'Juanita Guzmán',
      imagen: '../../assets/imgs/team/juanita.jpg',
      cargo: 'Ventas',
    }];

  let gerencia:Team[] = [{
      nombre: 'Alexander Ortiz',
      imagen: '../../assets/imgs/team/alex.jpg',
      cargo: 'CEO',
  }]

  let contenidos:Team[] = [{
    nombre: 'Daniela Ortiz',
    imagen: '../../assets/imgs/team/daniela.jpg',
    cargo: 'Analista de Contenidos',
  }];

  let departamentos:Departamento[]  =[{
      nombre: "Developer",
      team: developer,
      color: "developerColor",
      icon: 'logo-codepen'
    },{
      nombre: 'Creative',
      team: creative,
      color: "creativeColor",
      icon: "md-color-palette"
    },{
      nombre: "Contenidos",
      team: contenidos,
      color: "contenidosColor",
      icon: "md-book"
    },{
      nombre: "Gerencia",
      team: gerencia,
      color: "gerenciaColor",
      icon: 'md-globe'
    },{
      nombre: "Ventas",
      team: ventas,
      color: "ventasColor",
      icon: "ios-people"
      }];

  departamentos.forEach(departamento => {
    this.setDepartamento(departamento);
  });

}

}


export interface Team{
  nombre:string,
  cargo:string,
  imagen?:string,
 
}

export interface Departamento{
  nombre:string,
  color?:string,
  icon?:string,
  team:Team[]
  
}