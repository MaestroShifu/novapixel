import { Injectable } from '@angular/core';

/*
  Generated class for the ServiciosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiciosProvider {

  private servicios:Servicio[] = [];

  constructor() {
   this.start();
  }

  getServicios(){
    return this.servicios;
  }

  setServicio(servicio:Servicio){
    this.servicios.push(servicio);
  }

  getServiciosFind(index:number){
    return this.servicios[index];
  }

  start(){
    
  let recetaDiseno:Receta[] = [{
      titulo: "Levantamiento de Requerimientos",
      descripcion: "Entendiendo la necesidad de tu empresa, previo al diseño se realizará un levantamiento de requerimientos con el fin de listar y categorizar todos los elementos que permitan a nuestro equipo de trabajo adecuar tu solución. Se tendrán en cuenta datos técnicos, funcionalidad y alcance del proyecto, seleccionando los servicios principales, información, contenido del sitio, diseño y construcción de Base de Datos, y los recursos necesarios para el desarrollo.",
      imagen: "../../assets/imgs/servicios/recetas/paso-1.png"
    },{
      titulo: "Diseño y Desarrollo",
      descripcion: "Con base en la estructura y alcance del proyecto, se realizará el diseño y desarrollo del sitio web estableciendo un periodo para pruebas de usuario con el objetivo de alcanzar su estabilidad. Para esta fase se considerará la identificación y análisis de servicios complementarios que puedan formar parte de la adecuación del proyecto siguiendo los requerimientos de tu empresa.",
      imagen: "../../assets/imgs/servicios/recetas/paso-2.png"
    },{
      titulo: "Migración de la Información",
      descripcion: "En conjunto con tu equipo de trabajo, se realizará la migración de información al sitio web, teniendo en cuenta los requerimientos iniciales, la disponibilidad de los recursos y calidad del contenido existente. Revisaremos la información útil y relevante con el fin de captar la atención de tus clientes. También podrás adquirir servicios adicionales como campañas publicitarias, marketing digital y seguimiento de la interacción con tus clientes.",
      imagen: "../../assets/imgs/servicios/recetas/paso-3.png"
    },{
      titulo: "Identidad Corporativa",
      descripcion: "Para la entrega del proyecto se incluye una interfaz de usuario personalizada de acuerdo a la identidad corporativa de la organización y optimizada para visualizaciones desde cualquier dispositivo con acceso a internet. En esta fase se requerirá un previo estudio para lograr un diseño que se ajuste a los lineamientos de identidad corporativa, información del sitio web y contenido que estará disponible al cliente.",
      imagen: "../../assets/imgs/servicios/recetas/paso-4.png"
    }];

    let recetaAdministracionSocial:Receta[] = [{
      titulo: "Redacción de textos publicitarios",
      descripcion: "No solo es publicar actualizaciones, sino que debes conocer la mejor manera de que a través del lenguaje mantengas siempre abiertos los canales de comunicación con tus clientes. Al igual que la psicología del color, las palabras y su organización tienen impacto en  aquellos que leen tus publicaciones a través de tus redes sociales.",
      imagen: "../../assets/imgs/servicios/recetas/paso-1.png"
    },{
      titulo: "Elaboración de imágenes para publicaciones",
      descripcion: "No puedes negar que las imágenes son el medio de comunicación que prima en las redes sociales, es por ello que un equipo de creativos profesiones producirá imágenes atractivas y contundentes para tu público, y todo ello, sin nunca perder tu imagen corporativa.",
      imagen: "../../assets/imgs/servicios/recetas/paso-2.png"
    },{
      titulo: "Administración completa de tus redes sociales.",
      descripcion: "Sabemos que estas ocupado siendo el líder de tu proyecto empresarial y no siempre tienes el tiempo para administrar tus redes sociales y mantener contacto con tu público. Es por ello que en NovaPixel un profesional en la administración en las redes sociales se encargará de mantener y aumentar los canales comunicativos con tus clientes, además, de crear y administrar campañas publicitarias.",
      imagen: "../../assets/imgs/servicios/recetas/paso-3.png"
    },{
      titulo: "Uno de los sitos más visitados",
      descripcion: "Una vez llevados a cabo todos los pasos anteriores, verás como tus páginas  aumentarán considerablemente la interacción con tu público y el resultado y metas que has estado buscando.",
      imagen: "../../assets/imgs/servicios/recetas/paso-4.png"
    }];

    let recetaRediseno:Receta[] = [{
      titulo: "Diagnóstico",
      descripcion: "Nuestro equipo de expertos realizará un diagnóstico a tu sitio web para conocer el estado actual de este y enviarte una propuesta de rediseño, la cual vaya de acuerdo a tu imagen corporativa, necesidades  y publico objetivo.",
      imagen: "../../assets/imgs/servicios/recetas/paso-1.png"
    },{
      titulo: "Implementación",
      descripcion: "Una vez que hayas aprobado un diseño, NovaPixel realizará la planificación de trabajo para actualizar tu sitio web y entregartelo lo mas pronto posible con todas las mejoras.",
      imagen: "../../assets/imgs/servicios/recetas/paso-2.png"
    },{
      titulo: "¡Eres parte de nuestro equipo!",
      descripcion: "Durante todo el proceso de producción podrás estar al tanto de los procesos que realizamos para que siempre estes enterado de como se está rediseñando tu sitio web, todas tus sugerencias e ideas serán de suma importancia para que el producto final satisfaga las demandas de tu compañía.",
      imagen: "../../assets/imgs/servicios/recetas/paso-3.png"
    },{
      titulo: "Entrega",
      descripcion: "Al estar tu sitio web finalizado y que te sientas satisfecho con él, te haremos entrega de este para que lo pongas en uso lo más pronto posible. ¡Buen provecho!",
      imagen: "../../assets/imgs/servicios/recetas/paso-4.png"
    }];

    let recetaImagen:Receta[] = [{
      titulo: "Análisis de tu imagen",
      descripcion: "Nuestro equipo de trabajo tomará como referencia razón de ser de tu empresa, para identificar aquellos elementos que se pueden destacar en la construcción de tu identidad corporativa. Además, se tendrá en cuenta lo que tu empresa necesita: papelería comercial, membretes, tarjetas, sobres, rótulos etc.",
      imagen: "../../assets/imgs/servicios/recetas/paso-1.png"
    },{
      titulo: "Diseño y Construcción",
      descripcion: "Para estructurar tu identidad corporativa nuestro equipo de diseñadores plasmarán en el lienzo la mejor combinación de formas y colores para captar la atención de los clientes que interactúen con tu empresa. Al final de esta fase te entregaremos las propuestas necesarias para revisión.",
      imagen: "../../assets/imgs/servicios/recetas/paso-2.png"
    },{
      titulo: "Revisión de Propuestas",
      descripcion: "De acuerdo con las propuestas, en conjunto con tu equipo de trabajo se harán las revisiones correspondientes hasta construir la identidad que se ajuste a tus necesidades, con base en los requerimientos iniciales, tu logotipo y esquema visual de tu empresa.",
      imagen: "../../assets/imgs/servicios/recetas/paso-3.png"
    },{
      titulo: "Identidad Corporativa",
      descripcion: "Para la entrega del proyecto se tendrá todo un modelo de referencia que te permita ajustar la forma en que interactúas con tus clientes (impresión de papelería, membretes, tarjetas, etc). También se incluirá un manual de uso e impresión de tus formatos y archivos fuente.",
      imagen: "../../assets/imgs/servicios/recetas/paso-4.png"
    }];

    let recetaAdministracion:Receta[] = [{
      titulo: "Blogs y noticias",
      descripcion: "Subimos contenido informativo a tu Blog o sección de noticias en tu página web. Nuestro equipo de trabajo está altamente calificado en redacción de textos informativos, para garantizar una adecuada publicación de tus actualizaciones.",
      imagen: "../../assets/imgs/servicios/recetas/paso-1.png"
    },{
      titulo: "Eventos",
      descripcion: "Sabemos que para tu empresa es importante mantener constantemente actualizado el calendario de eventos, para que tus clientes siempre sepan que se avecina en cuanto a tus productos, promociones y cambios.",
      imagen: "../../assets/imgs/servicios/recetas/paso-2.png"
    },{
      titulo: "No importa lo que necesites, nosotros lo hacemos",
      descripcion: "Te ofrecemos distintos paquetes de ayuda para tu sitio web, para que sin importar cual se tu pedido puedas acceder a nuestros servicios, es decir, puedes contactarnos por una pequeñez o un gran proyecto.",
      imagen: "../../assets/imgs/servicios/recetas/paso-3.png"
    },{
      titulo: "Nos encargamos de tu sitio",
      descripcion: "Encárgate de liderar tus proyectos, que en NovaPixel gestionaremos el contenido de tu sitio web de la manera más profesional.",
      imagen: "../../assets/imgs/servicios/recetas/paso-4.png"
    }];

    let receta2:Receta[] = [{
      titulo: "Un servicio ideal para tu público",
      descripcion: "La cartelería digital resulta muy interesante para restaurantes, cadenas de comida, hospitales, tiendas de ropa etc. pues estas empresas atraen fácilmente en sus instalaciones a grandes grupos de personas, quienes hallarán muy agradable ver en tus pantallas información sobre eventos, promociones, datos curiosos, cambios y lo que desees compartir con ellos.",
      imagen: "../../assets/imgs/servicios/recetas/res-1.png"
    },{
      titulo: "Cartelería para grandes empresas",
      descripcion: "Las grandes compañias encontrarán en la cartelería digital una potente herramienta, pues estas al tener sucursales a lo largo y ancho del país, invierten constantemente recursos y tiempo en la efectiva transmisión de la información, tanto para sus clientes como para su equipo de trabajo, es por ello que en NovaPixel nos preocupamos por que esta sea llevada a tus pantallas de manera efectiva y contundente, y todo esto, sin nunca perder tu escencia corporativa. ",
      imagen: "../../assets/imgs/servicios/recetas/res-2.png"
    },{
      titulo: "Tu solo dinos qué quieres",
      descripcion: "No te preocupes por los dispositivos, pues en NovaPixel te proveemos no solo el software y la creación de piezas gráficas, sino que también el hardware necesario para que el servicio funcione de manera optima en todo momento. ",
      imagen: "../../assets/imgs/servicios/recetas/res-3.png"
    },{
      titulo: "¡Anímate!",
      descripcion: "Si ya estás listo para que tu empresa o negocio de el siguiente paso en calidad y atractivo para tus clientes, llámanos y construyamos juntos tu éxito. <p>¡NovaPixel tu aliado!</p>",
      imagen: "../../assets/imgs/servicios/recetas/res-4.png"
    }];

    let receta3:Receta[] = [{
      titulo: "Desarrollamos Apps a tu medida",
      descripcion: "Sabemos que en la actualidad la gran mayoría de usuarios utilizan como dispositivo de trabajo su móvil, es así como te ofrecemos la posibilidad de crear tu propia aplicación que responda a las necesidades de tu proyecto y que vaya a la vanguardia tecnológica.",
      imagen: "../../assets/imgs/servicios/recetas/des-1.png"
    },{
      titulo: "Software empresarial (CRM)",
      descripcion: "Entendemos que en el quehacer diario de tu compañía muchos de los procesos no se llevan a cabo de la manera más óptima, esto ocasiona que inviertas tiempo y recursos excesivos en dichas tareas. Es así como NovaPixel pone a tu disposición un software pensado para todas las áreas de tu empresa, que te permita gestionar y centralizar la información en un solo lugar.",
      imagen: "../../assets/imgs/servicios/recetas/des-2.png"
    },{
      titulo: "Facturar nunca fue tan fácil",
      descripcion: "¿Has tenido problemas con tu software de facturación? ¿Es demasiado costoso y el soporte es desastroso? No te preocupes, podemos desarrollar una herramienta personalizada para tus facturas, solo pregúntanos como lo hacemos.",
      imagen: "../../assets/imgs/servicios/recetas/des-3.png"
    },{
      titulo: "Plataformas educativas",
      descripcion: "Entendemos que la educación va de la mano con el crecimiento empresarial, es por ello que en NovaPixel estamos altamente calificados para desarrollar ambientes virtuales educativos, idóneos y exclusivos para tu empresa.",
      imagen: "../../assets/imgs/servicios/recetas/des-4.png"
    }];
    //asdasdas
    let recetaPublicidadDigital:Receta[] = [{
      titulo: "Google AdWords",
      descripcion: "Aparece en los primeros resultados de las busquedas en Google (google.com). Tu elijes dónde publicar tu anuncio, establece el presupuesto adecuado y mide el impacto de tu anuncio. No hay compromiso de inversión mínima y puedes detener o cancelar la publicación de tus anuncios en cualquier momento. Optimización constante de tus anuncios / pagina web por parte de NovaPixel",
      imagen: "../../assets/imgs/servicios/recetas/pub-1.png"
    },{
      titulo: "Facebook",
      descripcion: "Puede realizar anuncios de eventos, videos, fotos, promociones, etc. Dirige tus anuncios al publico adecuado, según sus intereses, edad, profesión, etc. Pagas por clic (CPC) o por conversión (CPA). No hay compromiso de inversión mínima y puedes detener o cancelar la publicación de tus anuncios en cualquier momento",
      imagen: "../../assets/imgs/servicios/recetas/pub-2.png"
    },{
      titulo: "Linkedin",
      descripcion: "Puede realizar anuncios de eventos, videos, fotos, promociones, etc. Dirige tus anuncios al publico adecuado, según sus intereses, edad, profesión, etc. Pagas por clic (CPC) o por conversión (CPA). No hay compromiso de inversión mínima y puedes detener o cancelar la publicación de tus anuncios en cualquier momento",
      imagen: "../../assets/imgs/servicios/recetas/pub-3.png"
    },{
      titulo: "Google AdWords",
      descripcion: "Aparece en los primeros resultados de las busquedas en Bing (Metabuscador de Microsoft - bing.com). Tu elijes dónde publicar tu anuncio, establece el presupuesto adecuado y mide el impacto de tu anuncio. No hay compromiso de inversión mínima y puedes detener o cancelar la publicación de tus anuncios en cualquier momento. Optimización constante de tus anuncios / pagina web por parte de NovaPixel",
      imagen: "../../assets/imgs/servicios/recetas/pub-4.png"
    },{
      titulo: "Waze",
      descripcion: "Aparece en los mapas de Waze Establece el presupuesto adecuado y mide el impacto de tu anuncio.  No hay compromiso de inversión mínima y puedes detener o cancelar la publicación de tus anuncios en cualquier momento. Optimización constante de tus anuncios / pagina web por parte de NovaPixel",
      imagen: "../../assets/imgs/servicios/recetas/pub-5.png"
    }];
    
    let servicios:Servicio[] = [
      {
        imagen: "../../assets/imgs/servicios/diseo-y-desarrollo-de-sitios-web.jpg",
        titulo: "Diseño y desarrollo de Sitios Web",
        recetas: recetaDiseno,
        descripcion: "En NovaPixel diseñamos el sitio web de tu empresa, usando los mas altos estándares de calidad, y sobre todo garantizando que sean atractivos para tus clientes, pero sin que pierdas la posibilidad de administrarla y manejarla en su totalidad. Elegimos la tecnología que más se ajuste a tus necesidades."
      },{
        imagen: "../../assets/imgs/servicios/imagen-corporativa.jpg",
        titulo: "Imagen Corporativa",
        recetas: recetaImagen,
        descripcion:"Con este servicio te ayudamos a satisfacer la necesidad de construir una contundente y fiel imagen corporativa que vaya de la mano con la misión y la visión empresarial que propones."
      },{
        imagen: "../../assets/imgs/servicios/administracin-de-sitios-web.jpg",
        titulo: "Administración de sitios web",
        recetas: recetaAdministracion,
        descripcion: "En NovaPixel tenemos una solución para ti, escríbenos lo que necesitas y un WebMaster experto se ocupara de tu solicitud: Actualización de contenidos, creación de entradas en blogs, eventos, promociones, y lo que necesites."
      },{
        imagen: "../../assets/imgs/servicios/publicidad-digital.jpg",
        titulo: "Publicidad Digital",
        recetas: recetaPublicidadDigital,
        descripcion: "La publicidad digital es actualmente la mejor  y más económica alternativa para llevar a cabo campañas publicitarias, pues esta te permite tener control sobre los públicos, inversiones y mucho más. "
      },{
        imagen: "../../assets/imgs/servicios/rediseo-o-actualizacion-web.png",
        titulo: "Rediseño y Actualización Web",
        recetas: recetaRediseno,
        descripcion: "NovaPixel entendiendo el fenómeno tecnológico por el que actualmente atraviesa la economía de las empresas colombianas pone a tu disposición este servicio, que permitirá que tu compañía acceda a la nueva era digital, teniendo como sitio web una elegante pero a un así, administrable herramienta, que te permita tener control de las interacciones con tus clientes manteniendo tu identidad corporativa."
      },{
        imagen: "../../assets/imgs/servicios/administracion-de-redes-sociales.png",
        titulo: "Administración de redes sociales",
        recetas: recetaAdministracionSocial,
        descripcion: "Las  redes sociales, actualmente son el principal medio de interacción con tus clientes. En NovaPixel ponemos a tu disposición a nuestro equipo de especialistas, quienes pueden administrar tus redes sociales, alimentar tu blog e incluso hacer tus campañas publicitarias."
      },{
       imagen: "../../assets/imgs/servicios/cloud.png",
       titulo: "Desarrollo de software - Cloud",
       recetas: receta3,
       descripcion: "NovaPixel conoce que tu empresa o compañía es única al igual que tus necesidades, es por ello que te ofrecemos un servicio hecho a tu medida, ya que sabemos que en la era de la economía digital necesitas un soporte virtual, para llevar acabo tu quehacer diario de manera óptima y administrable." 
      },{
      imagen: "../../assets/imgs/servicios/cartwe.png",
      titulo: "Cartelería Digital",
      recetas: receta2,
      descripcion: "La cartelería digital surge como una solución potente y economica para llevar la información visual a tus consumidores y equipo de trabajo. Este mecanismo publicitario y comunicativo, no es solamente una pantalla que muestra información relevante, sino que se ha convertido en un atractivo método de llamar la atención de tus clientes y conectarlos de manera agradable con tus productos o servicios."
      }
    ];

    servicios.forEach(servicio => {
      this.setServicio(servicio);
    });
  }

}

export interface Servicio{
  imagen:string,
  titulo:string,
  descripcion:string,
  recetas?:Receta[]
}

export interface Receta{
  titulo: string,
  descripcion: string,
  imagen: string
}
