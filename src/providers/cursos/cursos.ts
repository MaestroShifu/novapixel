import { Injectable } from '@angular/core';

/*
  Generated class for the CursosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CursosProvider {

  private cursos:Curso[] = [];

  constructor() {
    this.start();
  }

  start(){
    let cursos:Curso[] = [{
        nombre: "Marketing Digital",
        duracion: "Toma el entrenamiento en 20 horas : 20 horas presenciales",
        imagen: "../../assets/imgs/cursos/marketingDigital.jpg",
        descripcion: "Fortalecer las competencias, prácticas y aprender a construir estrategias rentables sostenibles.",
        examen: "http://certmind.org/index.php/certifications/digital-marketing/certified-social-manager",
        simuladores: "Al final del curso presentas los simuladores de examen: 200 preguntas para fortalecer tu conocimiento.",
        modalidad: "No hay excusas: Este entrenamiento está disponible en la modalidad Presencial u Online (Clases en vivo).",
        certificacion: "Recibiras un certificado internacional por parte de CertMind.",
      },{
        nombre: "ISTQB",
        duracion: "Toma el entrenamiento en 30 horas: 20 horas presenciales +  10  en plataforma virtual .",
        imagen: "../../assets/imgs/cursos/istqb.png",
        descripcion: " El ISTQB (International Software Testing Qualifications Board) es una organización de certificación de la calidad del software que opera internacionalmente.",
        examen: "",
        simuladores: "Al final del curso presentas los simuladores de examen: +280 preguntas para fortalecer tu conocimiento.",
        modalidad: "No hay excusas: Este entrenamiento está disponible en la modalidad Presencial u Online (Clases en vivo)",
        certificacion: "Este entrenamiento incluye el examen de certificación internacional. Válido en más de 176 países",

      },{
        nombre: "DevOps",
        duracion: "Toma el entrenamiento en 20 horas : 20 horas presenciales + simuladores en plataforma académica.",
        imagen: "../../assets/imgs/cursos/devops.png",
        descripcion: "Interacción continua, mejores practicas para el desarrollode software y manejo equipos.",
        examen: "http://certmind.org/index.php/certifications/agile-software/devops-fundamentals",
        simuladores: "Al final del curso presentas los simuladores de examen: +280 preguntas para fortalecer tu conocimiento.",
        modalidad: "No hay excusas: Este entrenamiento está disponible en la modalidad Presencial u Online (Clases en vivo).",
        certificacion: "Este entrenamiento incluye el examen de certificación internacional. Válido en más de 176 países",

      },{
        nombre: "Scrum Gold",
        duracion: "Toma el entrenamiento en 40 horas : 24 horas presenciales +  16 simuladores en plataforma académica.",
        imagen: "../../assets/imgs/cursos/scrum.jpg",
        descripcion: "Adapta la metodología ágil a tus proyectos y optimiza los procesos de creación y entrega de tus producctos.",
        examen: "http://certmind.org/index.php/certifications/digital-marketing/certified-social-manager",
        simuladores: "Al final del curso presentas los simuladores de examen: +280 preguntas para fortalecer tu conocimiento.",
        modalidad: "No hay excusas: Este entrenamiento está disponible en la modalidad Presencial u Online (Clases en vivo).",
        certificacion: "Este entrenamiento incluye el examen de certificación internacional. Válido en más de 176 países",
      },
    ];

    cursos.forEach(curso => {
      this.setCursos(curso);
    });

    console.log("Se cargaron los cursos satisfactoriamente");
  }

  getCursos(){
    return this.cursos;
  }

  setCursos(curso:Curso){
    this.cursos.push(curso);
  }

  getCursoFind(index:number){
    return this.cursos[index];
  }
}

export interface Curso{
  nombre:string,
  duracion:string,//
  imagen:string,
  descripcion:string,
  examen?:string,
  simuladores?:string,//
  certificacion?:string,
  modalidad?:string
}
