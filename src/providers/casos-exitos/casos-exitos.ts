import { Injectable } from '@angular/core';

/*
  Generated class for the CasosExitosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CasosExitosProvider {

  private casos:Casos[] = [];

  constructor() {
    this.start();
  }

  start(){
    let casos:Casos[] = [{
      nombre: "Gesthor® Software de Gestión Empresarial",
      imagen: "../../assets/imgs/casos/GESTHOR.jpg",
      descripcion: "Gesthor® es un Software de Gestión Empresarial que permite aumentar la productividad de tus empleados, automatizar los procesos, centralizar la información y lo mejor, todo en una sencilla pero hermosa interfaz gráfica. \n NovaPixel durante todo el proceso creativo de Gesthor®, mantuvo como norte la optimización de las tareas diarias empresariales, dicho esto, cada uno de sus 25 módulos que componen la anatomía de Gesthor® suponen un considerable aumento en la productividad en los procesos. Estos últimos siendo los más completos en el mercado actual, ya que satisfacen todos los ámbitos del quehacer diario corporativo.",
      fecha: " 25/11/16",
      trabajo: "Desarrollo"
    },{
      nombre: "¡Eureka!",
      imagen: "../../assets/imgs/casos/EUREKA.jpg",
      descripcion: "¡Eureka! nace como respuesta a la insatisfacción educativa demandada por las necesidades técnicas y operativas  en el ambito empresarial de Colombia, es por ello que en NovaPixel desarrollamos una plataforma pensada para el optimo aprendizaje de los individuos, para que  de manera interactiva y aprovechando las posibildiades que brinda las tecnológias de la información, se generen procesos educativos, y así favorecer la economía, el emprendimiento y el campo laboral en Colombia. \n Nuestra plataforma cuenta con una sección de ofertas laborales para contribuir a que los sujetos tengan una exitosa integración a la vida laboral.",
      fecha: " 25/11/16",
      trabajo: "Desarrollo"
    },{
      nombre: "Encuadernación Chapinero",
      descripcion: "NovaPixel inclina gran parte de sus esfuerzos en el emprendimiento colombiano, ofreciendo planes y paquetes accesibles a la pequeña y mediana empresa también. Es así como en colaboración con Encuadernación Chapinero se lleva a cabo todo un proceso de creación de imagen corporativa, que capturarese la escencia de la misma. Además de esto y entendiendo el fenomeno de tecnologías de la información que está atravesando el país, se desarrolló exitosamente un sitio web, atractivo, funcional y práctico que acortase las distancias comunciativas entre posibles clientes y la empresa, y que además permitiese la debida administración y analisis de datos.",
      imagen: "../../assets/imgs/casos/encuadernacion.png",
      fecha: " 25/11/16",
      trabajo: "Páginas Web, Imagen corporativa"
    },{
      nombre: "MultipleSkills",
      imagen: "../../assets/imgs/casos/multipleskills.jpg",
      descripcion: "NovaPixel desarrolló y culminó en su totalidad una plataforma educativa integral para MultipleSkills, siendo también creadores del contenido de dicha iniciativa. Entendiendo que el fenomeno educativo esta atravesado por las tecnologías de la información, tanto la plataforma como sus contenidos aprovechan las posibildiades actuales, garantizando un exitoso proceso formativo.\n La plataforma fue desarrollada con la más actual tecnología para garantizar su funcionamiento optimo,sin perder calidad estetica, lo cual caracteriza los proyectos realizados por NovaPixel.",
      fecha: " 25/11/16",
      trabajo: " Desarrollo de contenido"
    },{
      nombre: "Findeter",
      imagen: "../../assets/imgs/casos/finderter.jpg",
      descripcion: "NovaPixel entiende las necesidades de las empresas colombianas, por eso pone a su disposición novedosos productos y servicios para que estas también vayan a la vanguardia. Es así como ofrecemos la cartelería digital, un sistema de comunicaciones interconectadas a través de todo el país que permiten así, la correcta y agradable transmisión comunicativa. Este mecanismo no solo es más práctico, veloz y atractivo para los espectadores, sino que también es amigable con el medio ambiente y permite reducir costos para la empresa.\n Findeter (al igual que muchas otras empresas colombianas) siendo una entidad estatal con varias regionales ubicadas a lo largo y ancho del país, comprende y hace uso de las posibilidades de la cartelería digital a través de los servicios de NovaPixel.",
      fecha: " 06/12/16",
      trabajo: "Cartelería digital"
    },{
      nombre: "CCTI Soluciones Tecnológicas",
      imagen: "../../assets/imgs/casos/CCTI.jpg",
      descripcion: "NovaPixel ofrece a CCTI desde hace ya varios años un paquete de servicios en publicidad, diseño y soporte. Todos estos servicios se prestan cuidando siempre de mantener la calidad e imagen corporativa que ha caracterizado a CCTI desde siempre. Adicional a esto, en NovaPixel estamos en constante busqueda de propuestas innovadoras para que su imagen y productos siempre estén actualizados para los clientes, asegurandonos de que la información y los servicios se desarrollen de manera efectiva.",
      fecha: " 03/08/18",
      trabajo: "Desarrollo de contenido, Imagen corporativa"
    },{
      nombre: "Grupo KBG",
      imagen: "../../assets/imgs/casos/KBG.jpg",
      descripcion: "Uno de los servicios ofrecidos por NovaPixel dentro de la construcción de imagen corporativa es la creación de tarjetas profesionales para empresarios y grupos empresariales. Es así como el grupo empresarial KBG deposita su confianza en NovaPixel para el diseño de todas sus tarjetas de negocios, capturando así su escencia y propuesta de valor como grupo empresarial, para que desde su imagen corporativa se desarrolle un sentido de pertenencia y reconocimiento de la marca por parte de sus clientes.",
      fecha: " 03/08/18",
      trabajo: "Imagen corporativa"
    },{
      nombre: "Academia de Billares Colombia",
      imagen: "../../assets/imgs/casos/billars.jpg",
      descripcion: "En NovaPixel sabemos que desarrollar un concepto visual que de rostro a la imagen corporativa de un empresa es de vital importancia, pues construir una marca atractiva y fiel a tu razón empresarial es una de las grandes metas de toda compañia.\n NovaPixel lideró este proyecto de construcción visual al rededor del quehacer de la Academia de Billares Colombia, recogiendo su escencia y peculiriades unicas, para así dar forma a su marca.",
      fecha: " 03/08/18",
      trabajo: "Imagen corporativa"
    },{
      nombre: "Lanufo.com: La nueva forma de comprar",
      imagen: "../../assets/imgs/casos/lanufo.jpg",
      descripcion: "Lanufo.com es un sitio de E-comerce que promueve la comidad al comprar en su sitio para los usuarios, es por ello que NovaPixel, luego de un arduo estudio de marca, razon empresarial, metas y publico objetivo, desarrolló un entramado visual que dio forma a la marca corporativa de lanufo.com",
      fecha: " 03/08/18",
      trabajo: "Desarrollo, Páginas Web"
    },{
      nombre: "Reensabana",
      imagen: "../../assets/imgs/casos/REENSABANA.jpg",
      descripcion: "NovaPixel está siempre disponible para tus proyectos, sin importar la complejidad de tus pedidos, es así como para la Reencauchadora De La Sabana, realizamos infografías, que mantuvieran la identidad corporativa y razón de empresarial, y sobretodo que cumplieran con su proposito informativo.",
      fecha: " 06/08/18",
      trabajo: "Imagen corporativa"
    }
  ];

  casos.forEach(caso => {
    this.setCasos(caso);
  });

  }

  getCasos(){
    return this.casos;
  }

  setCasos(caso:Casos){
    this.casos.push(caso);
  }

  getCasoFind(index:number){
    return this.casos[index];
  }
 
}

  export interface Casos{
    nombre:string,
    imagen?:string,
    descripcion:string,
    fecha:string,
    trabajo:string
  }
